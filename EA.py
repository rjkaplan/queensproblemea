import random
import sys
NUMQUEENS = 8
POPULATIONSIZE = 5
NUMTRIALS = 50

def allTheSame(L):
    # returns True if L is full of identical
    # elements
    return all(x == L[0] for x in L)

def queensCollide(q1, q2):
    # returns True is q1 (a pair of numbers)
    # is in 'diagonal-check' with q2
    if (q1[0] - q1[1] == q2[0] - q2[1]):
        return True
    if (q1[0] + q1[1] == q2[0] + q2[1]):
        return True
    return False

def numCollisions(perm):
    # returns the number of 'checks'
    # in a permutation representing the
    # positions of queens on the board
    count = 0
    for i in range(len(perm)):
        for j in range(i+1, len(perm)):
            if queensCollide((i, perm[i]), (j, perm[j])):
                count += 1
    return count

def crossover(p1, p2):
    # crosses over two parent permutations
    crossoverpoint = random.randrange(NUMQUEENS)
    c1 = p1[:crossoverpoint]
    c2 = p2[:crossoverpoint]
    c1.extend(filter(lambda x: x not in c1, p2))
    c2.extend(filter(lambda x: x not in c2, p1))
    return (c1, c2)

def generateIndividual():
    L = range(NUMQUEENS)
    random.shuffle(L)
    return L

def main():
    # generate population
    population = [generateIndividual() for i in range(POPULATIONSIZE)]

    for i in range(NUMTRIALS):
        # pick two parents
        parent1 = random.randrange(len(population))
        parent2 = random.randrange(len(population))
        while parent1 == parent2:
            parent2 = random.randrange(len(population))
       
        parent1 = population[parent1]
        parent2 = population[parent2]

        # perform crossover
        children = crossover(parent1, parent2)
        population.extend(children)
        values = map(numCollisions, population)

        # find two maximum values and remove them
        for i in range(2):
            minIndex = max(xrange(len(values)), key=values.__getitem__)
            del values[minIndex]
            del population[minIndex]

        if allTheSame(population):
            print ("We found a local optimum, which is a permutation with %i collisions" % (numCollisions(population[0])))
            print "It is: ", population[0], "\n"
            break 

    print "The resultant population is:"
    for member in population:
        print member
    return min(map(numCollisions, population))

if __name__ == "__main__":
    if len(sys.argv) < 3 or not (sys.argv[1].isdigit() and sys.argv[2].isdigit()):
        print "Usage: python EA.py <number of queens> <number of trials>"
        sys.exit(0)
    NUMQUEENS = int(sys.argv[1])
    NUMTRIALS = int(sys.argv[2])
    if NUMQUEENS == 1:
        # We'll end up looping 4-eva
        print "This is a silly problem."
        sys.exit(0)
    main()
